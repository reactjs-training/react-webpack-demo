# README #

A sample react app using webpack

### What is this repository for? ###

* Webpack learner

### How do I get set up? ###

#### 1. Install All dependency

`npm install`

#### 2. Build project

`npm run build`

#### 3 Start server

`npm run start
`