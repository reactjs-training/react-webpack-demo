import React from 'react';
import ReactDOM from 'react-dom';

import Form from "./js/components/Form";

import img from "./img/process.png";

import "./scss/app.scss";

const App = () => (
  <div className="app">
     <h1>Hello world!!</h1>
     <Form />
     <img src={img}></img>
  </div>
);


ReactDOM.render(<App/>, document.getElementById('container'));
