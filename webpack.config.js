const path = require('path');
const webpack = require("webpack");
const fs = require("fs");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

config = {
  source: "/src",
  target: "/build"
}

module.exports = {
  entry: path.join(__dirname, `${config.source}/index.js`),
  output: {
      filename: 'build.js',
      path: path.join(__dirname, config.target)
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader'
          },
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: {
          loader: "file-loader",
          options: {
            name: "static/media/[name].[hash:8].[ext]"
          }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: `.${config.source}/index.html`
    }),
    new MiniCssExtractPlugin({
      filename: "style.css"
    }),
    new webpack.BannerPlugin(fs.readFileSync("./LICENSE", "utf8"))
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin()
    ],
  },
};